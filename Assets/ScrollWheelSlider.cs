﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ScrollWheelSlider : MonoBehaviour
{
    public float scrollConstant = 0.01f;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Scroll wheel attached");
    }

    void OnGUI()
    {
        Event e = Event.current;
        //Debug.Log("Gui event " + e.type.ToString());
        if( e.type == EventType.ScrollWheel )
        {
            var slider = GetComponent<Slider>();
            slider.value = Mathf.Clamp( slider.value - e.delta.y * scrollConstant, 0, 1 );
        }
    }
}
