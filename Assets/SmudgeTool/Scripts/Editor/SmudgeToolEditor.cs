﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SmudgeTool))]
public class SmudgeToolEditor : Editor
{

  //
  // Runs when the GameObject with an attached SmudgeTool component attached.
  // The GUILayout, EditorGUILayout and EditorGUIUtility can be helpful.
  //   https://docs.unity3d.com/ScriptReference/GUILayout.html
  //   https://docs.unity3d.com/ScriptReference/EditorGUILayout.html
  //   https://docs.unity3d.com/ScriptReference/EditorGUIUtility.html
  //
  public override void OnInspectorGUI() {

    // The smudgeTool the editor is currently inspecting. 
    SmudgeTool smudgeTool = (SmudgeTool)target;

    // Could do something fancy here, but for now just draw the default inspector.
    DrawDefaultInspector();

  }

}
