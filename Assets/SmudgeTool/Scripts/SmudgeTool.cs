﻿// Notes:

// To implement hardness (or a lack thereof) defined as less than 100% contribution from texels as we smudge,
// we need texels to remember their contributions from all the other texels in the smudge.
// This means a single displacement map + original texture doesn't cut it; we need to rerender the texture
// as we smudge.
// (The displacement map is therefore unnecessary at this point, as one shader could calculate the displacement and
// resample itself, but the logical separation appeals to me.)

// I thought implementing an undo/redo stack would show that I went above and beyond the requirements of the test.
// I chose to optimize for memory rather than speed with my implementation of an undo/redo stack, keeping a buffer of all
// the operations a user makes using the tool.  Unfortunately it means undo/redo are not as snappy as I would like, particularly
// after painting a while. Adding sound effects for undo/redo helped create the illusion of less latency but was something of a band-aid.

// In the real world I would have asked the artists on the team what improvements to the tool they'd want; for this test
// however I decided to implement the ability to change the size of the brush with the mouse scrollwheel as my improvement to workflow.

// I also implemented a GUI, as you can see, and changed the brush preview to show the hardness function of the brush. (I'm not sure that's an improvement. :P)

// Future improvements. After seeing the tool in use we might decide that:
//  * We want to save a history of images rather than a history of operations for snappier undo/redo,
//    or cache the very last undo/redo so at least that operation is snappy

//  * We want to have a fixed length undo buffer, discarding the early entries after it becomes full, to cap the performance hit

//  * Use an ellipse from the smudge start point to end point instead of a circle - rapid mouse movements can cause a sort of 'scalloping' effect that
//    may not be desired (or may be, it could be considered more expressive)

//  * It felt like a premature abstraction at this point, but if someday the tool became a part of an entire paint program suite 
//    I'd imagine an architecture where each tool, the quad preview window, and the undo system were separate components.
//    Each tool would inherit from a base PaintSystemTool and the command for the undo buffer stack from a base PaintCommand component. 
//    The PaintCommand component would likely contain the shaders and a lot of the difference between multiple tools would simply be their
//    shader code. 


// This was a cool test!  I am a firm believer in having applicants to a job do something like the job they're applying for as part of their evaluation, 
// which seems shockingly rare in software engineering, so kudos!

// -Jamie 

using UnityEngine;
using UnityEngine.EventSystems;

using System;
using System.Collections.Generic;



public class SmudgeTool : MonoBehaviour {

  public Material SmudgeOperator;
  public Material SmudgeRenderer;


  // undo buffer
  public RedoStack m_redoStack = new RedoStack();
  public int undoIndex = 0;  // when undoing, we move this pointer back in the smudge stack; everything before this index is undo, everything after is redo

  // Holds a displacement vector per-pixel.
  private RenderTexture m_displacementMap;
    
  private RenderTexture m_undoTexture;

  private RenderTexture m_smudgeResultTexture;

  // The last mouse position, used in the shader to compute new displacement vectors for all pixels
  // under the current brush location.
  private Vector3 m_lastMousePos;

  private bool mouseButtonDownLastFrame = false;
  private bool trackingSmudge = false;

  // pretty hacky, because I'm building upon reading the keycode in update
  // given unlimited time I'd take the time to build a hotkey system into the UI buttons
  // but sometimes it's good to leave a little technical debt unpaid; you never know for sure if a feature is going
  // to catch on or be mothballed
  private bool undoKeyDownLastFrame = false;
  private bool redoKeyDownLastFrame = false;



  public class SmudgeOperation 
  {
    public Vector2 smudgeStart;
    public Vector2 smudgeEnd;
    public float size;
    public float hardness;
    public SmudgeOperation( Vector2 _start, Vector2 _end, float _size, float _hardness ) 
    {
      smudgeStart = _start;
      smudgeEnd = _end;
      size = _size;
      hardness = _hardness;
    }

    public void ExecuteOp( SmudgeTool smudgeTool )
    {
      smudgeTool.SmudgeOperator.SetVector("_LastMousePos", smudgeStart);
      smudgeTool.SmudgeOperator.SetVector("_MousePos", smudgeEnd);
      smudgeTool.SmudgeOperator.SetFloat("_Size", size);
      smudgeTool.SmudgeOperator.SetFloat("_Hardness", hardness);

      // The displacement map is no longer updated by accumulating new displacement vectors; it's a displacement for a single frame and no longer
      // technically necessary because we could simply calculate the new smudged texture from the old in one shader

      // However, a texture can't be safely read and written at the same, (i.e. we shouldn't do Graphics.Blit(m_displacementMap, m_displacementMap, SmudgeOperator))
      // so make a copy of it as
      // the source.
      var displacementCopy = RenderTexture.GetTemporary( smudgeTool.m_displacementMap.descriptor );
      Graphics.Blit( smudgeTool.m_displacementMap, displacementCopy);

      // Compute the new displacements using the smudge operator.
      // In the SmudgeOperator, "displacementCopy" becomes "_MainTex" and m_displacementMap stores
      // the output of the fragment shader.
      Graphics.Blit( displacementCopy, smudgeTool.m_displacementMap, smudgeTool.SmudgeOperator);

      // Release the temporary copy.
      RenderTexture.ReleaseTemporary( displacementCopy );

      // Render the new smudged texture
      // same deal as far as read/write at same time goes as displacement map
      var smudgeCopy = RenderTexture.GetTemporary( smudgeTool.m_smudgeResultTexture.descriptor );
      Graphics.Blit( smudgeTool.m_smudgeResultTexture, smudgeCopy );
      Graphics.Blit( smudgeCopy, smudgeTool.m_smudgeResultTexture, smudgeTool.SmudgeRenderer );
      RenderTexture.ReleaseTemporary( smudgeCopy );
    }  
  }

  // in a full paint suite program this would inherit from a generic PaintSuiteCommand
  public class SmudgeCommand: List<SmudgeOperation>
  {
    public void Execute( SmudgeTool smudgeTool )
    {
      foreach( var smudgeOp in this )
      {
        smudgeOp.ExecuteOp( smudgeTool );
      }
    }
  }

  public class RedoStack: List<SmudgeCommand>
  {
  }


  // Runs once, when the MonoBehaviour starts.
  void Start() {
    // Make a displacement map the same size as the current target texture.
    var mat = GetComponent<MeshRenderer>().material;
    m_displacementMap = new RenderTexture(mat.mainTexture.width,
                                       mat.mainTexture.height,
                                       0,
                                       RenderTextureFormat.ARGBFloat,
                                       RenderTextureReadWrite.Linear);
    m_displacementMap.useMipMap = false;

    m_smudgeResultTexture = new RenderTexture(mat.mainTexture.width,
                                    mat.mainTexture.height,
                                    0,
                                    RenderTextureFormat.ARGB32 );

    m_undoTexture = new RenderTexture(mat.mainTexture.width,
                                       mat.mainTexture.height,
                                       0,
                                       RenderTextureFormat.ARGB32 );

    // Zero out the displacement map.
    RenderTexture.active = m_displacementMap;
    GL.Clear(true, true, Color.black);
    
    Graphics.Blit( mat.mainTexture, m_smudgeResultTexture );
    Graphics.Blit( mat.mainTexture, m_undoTexture );

    RenderTexture.active = null;

    // Assign the displacement map to the main material and smudge operator.
    mat.SetTexture( "_MainTex", m_smudgeResultTexture );
    mat.SetTexture("_Displacement", m_displacementMap);
    SmudgeOperator.SetTexture("_Displacement", m_displacementMap);
    SmudgeRenderer.SetTexture("_Displacement", m_displacementMap);
  }

  void RedoOutputTexture()
  {
    // start over from the beginning
    RenderTexture.active = m_smudgeResultTexture;
    Graphics.Blit( m_undoTexture, m_smudgeResultTexture );
    RenderTexture.active = null;

    for( int i = 0; i < undoIndex; i++ )
    {
      var smudge = m_redoStack[i];
      smudge.Execute( this );
    }
  }

  // Runs on every frame.
  void Update() {
    
    // Convert the mouse position from screen space to world space.
    var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

    // Calculate the mouse distance from the center of the quad.
    // After this, the resulting mouse position is in NDC coordinates [-1, 1]
    mousePos.x = (mousePos.x - transform.position.x) / (transform.localScale.x / 2.0f);
    mousePos.y = (mousePos.z - transform.position.z) / (transform.localScale.z / 2.0f);

    // Convert NDC coordinates to normalized UV coords [0, 1]
    mousePos = mousePos * .5f + Vector3.one * .5f;

    // I considered making it to it would only respond to mouse events inside the quad
    // but I think that would make it less expressive for the artist

    // Update the mouse position input to the shader.
    var mat = GetComponent<MeshRenderer>().sharedMaterial;
    mat.SetVector("_MousePos", mousePos);

    // not doing ctrl-z, ctrl-y because it confuses the unity editor
//    if (Input.GetKey(KeyCode.LeftControl)||Input.GetKey(KeyCode.RightControl)) {
    if (Input.GetKey(KeyCode.Z) ) {
      if( !undoKeyDownLastFrame && undoIndex > 0 ) {
        Undo();
      }
      undoKeyDownLastFrame = true;
    }
    else
    {
      undoKeyDownLastFrame = false;
      if (Input.GetKey(KeyCode.Y) ) {
        if( !redoKeyDownLastFrame && undoIndex < m_redoStack.Count ) {
          Redo();
        }
        redoKeyDownLastFrame = true;
      }
      else {
        redoKeyDownLastFrame = false;
      }
    }

    if (Input.GetMouseButton(0)) {
      if( !mouseButtonDownLastFrame ) {
        if( !EventSystem.current.IsPointerOverGameObject() ) {
          // copy current texture for undo purposes
          Debug.Log("Starting new smudge");
          trackingSmudge = true;
          m_lastMousePos = mousePos;  // make sure there's no sudden jump, which could happen if you were mucking with a sidebar and then clicked on quad
          
          // destroy redo chain
          m_redoStack.RemoveRange( undoIndex, m_redoStack.Count - undoIndex );

          m_redoStack.Add( new SmudgeCommand() );
          undoIndex = m_redoStack.Count;
        }
      }
      if( trackingSmudge ) {
        var hardness= mat.GetFloat("_Hardness");
        //Debug.Log( "Smudging hardness " + hardness.ToString() );
        mouseButtonDownLastFrame = true;
        if( mousePos != m_lastMousePos )
        {
          var curSmudge = m_redoStack[ m_redoStack.Count - 1 ];
          var smudgeOp = new SmudgeOperation( 
            m_lastMousePos,
            mousePos,
            mat.GetFloat("_Size"),
            mat.GetFloat("_Hardness"));
          curSmudge.Add( smudgeOp );
          smudgeOp.ExecuteOp( this );
        }
      }
    }
    else {
      mouseButtonDownLastFrame = false;
      trackingSmudge = false;
    }

    // Store the last mouse position for the next update.
    m_lastMousePos = mousePos;
  }

  public void Undo() {
    Debug.Log("Undoing");
    GetComponent<AudioSource>().Play();
    undoIndex = Math.Max( undoIndex - 1, 0 );
    RedoOutputTexture();
  }

  public void Redo() {
    Debug.Log("Redoing");
    GetComponent<AudioSource>().Play();
    undoIndex = Math.Min( undoIndex + 1, m_redoStack.Count );
    RedoOutputTexture();
  }

  public void SetHardness( float hardness ) {
    var mat = GetComponent<MeshRenderer>().sharedMaterial;
    mat.SetFloat( "_Hardness", hardness );
  }

  public void SetSize( float size ) {
    var mat = GetComponent<MeshRenderer>().sharedMaterial;
    mat.SetFloat( "_Size", size );
  }


}
