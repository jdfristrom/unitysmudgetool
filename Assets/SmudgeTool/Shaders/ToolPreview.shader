﻿Shader "SmudgeTool/ToolPreview"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _MousePos ("Mouse Position", Vector) = (0, 0, 0, 0)
        _Size ("Size", Range(0, 1)) = 1
        _Hardness ("Hardness", Range(0, 1)) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 screenPos : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_TexelSize;
            float4 _MainTex_ST;

            sampler2D _Displacement;

            float2 _MousePos;
            float _Size;
            float _Hardness;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.screenPos = ComputeScreenPos(o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);

                // Determine the correction for non-square textures.
                float2 aspect = _MainTex_TexelSize.zw / max(_MainTex_TexelSize.z, _MainTex_TexelSize.w);
                
                // Compute the distance to the center of the brush circle.
                float dist = length((i.uv - _MousePos.xy) * aspect);

                // Ring size.
                float size = _Size * .1;

                // The first smoothstep below anti-aliases the inner edge of the ring, the second smoothstep
                // anti-aliases the outer edge.
                // col = lerp(/* Texture Color */ col,
                //            /* Ring Color    */ lerp(col * col, 1-col * 1-col, .4),
                //            smoothstep(size * .95, size * .96, dist) * (1 - smoothstep(size * .99, size, dist)));

                float closenessToCenter = saturate(1 - dist / size);  // 0 at edge, 1 at center

                // how much the texel we smudged from contributes to this texel - it's always 1 in the center and
                // diminishes to hardness at the edge. 
                float contribution = lerp( _Hardness, 1, smoothstep( 0, 1, closenessToCenter ));

                col = lerp(/* Texture Color */ col,
                           /* Ring Color    */ lerp(col * col, 1-col * 1-col, .4),
                           (1 - smoothstep(size * .99, size, dist)) * contribution * 0.5 );


                return col;
            }
            ENDCG
        }
    }
}
