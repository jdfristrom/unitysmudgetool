﻿Shader "SmudgeTool/SmudgeRenderer"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 screenPos : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_TexelSize;
            float4 _MainTex_ST;

            sampler2D _Displacement;

            float2 _MousePos;
            float _Size;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.screenPos = ComputeScreenPos(o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // Displacement is computed in the SmudgeOperator shader and stored in this texture.
                float4 displacementInfo = tex2D(_Displacement, i.uv);
                float2 displacement = displacementInfo.xy;
                float blend = displacementInfo.z;

                // Sample the texture color using the current UV + the displacement vector.
                fixed4 smudgeCol = tex2D(_MainTex, i.uv + displacement);
                fixed4 origCol = tex2D(_MainTex, i.uv);

                fixed4 col = lerp( origCol, smudgeCol, blend );

                return col;
            }
            ENDCG
        }
    }
}
