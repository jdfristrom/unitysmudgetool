﻿Shader "SmudgeTool/SmudgeOperator"
{
    Properties
    {
      _MainTex("Texture", 2D) = "white" {}
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass // 0 - Smudge
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            float4 _MainTex_TexelSize;
            float2 _MousePos;
            float2 _LastMousePos;
            float _Size;
            float _Hardness;

            fixed4 frag(v2f i) : SV_Target
            {
                // Correct for the image aspect ratio.
                float2 aspect = _MainTex_TexelSize.zw / max(_MainTex_TexelSize.z, _MainTex_TexelSize.w);
                float dist = length((i.uv.xy - _MousePos.xy) * aspect);

                // Compute the distance from this pixel to the center of the brush.
                float size = _Size * .1;
                float closenessToCenter = saturate(1 - dist / size);  // 0 at edge, 1 at center

                // how much the texel we smudged from contributes to this texel - it's always 1 in the center and
                // diminishes to hardness at the edge. 
                float contribution = lerp( _Hardness, 1, smoothstep( 0, 1, closenessToCenter ));
                // Compute the displacement vector and blend amount

                // an ellipse or capsule would be better; wishlist fix
                return float4( (_LastMousePos - _MousePos) * closenessToCenter * 0.8, contribution, 0 );  // x & y - displacmenet,z - blend

                // now we rerender every frame instead of accumulating into displacement map: + tex2D(_MainTex, i.uv + vec), 0);  // accumulates blend as well as displacement
            }
            ENDCG
        }

    }
}
