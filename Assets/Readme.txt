
Creating a Smudge Tool
======================

Create an artist-friendly a tool to smudge pixels in a texture.

A real tool would run at edit time, but for this toy project, the tool will
only be available while the project is running in play-mode.

You should approach this project as if you were working on a minimum viable
product being built in a team setting. The code should be minimal, easy to
read, and easy to build upon. Ambiguities or confusing aspects should be
explained, so the rest of your team can understand the code.

Please comment on any problems you see, even if you don't end up addressing
them.

Feel free to ask clarifying questions, if needed.


Project setup:

 * Open Assets/SmudgeTool/SmudgeTestScene.unity to begin.
 * Click run to start the project.
 * Clicking on the image smudges the texture pixels on the Quad object.
 * The material on the quad has a _Size parameter to adjust the size of the brush.

Requirements:

 * In C#, create an undo system to undo smudges.
 * Add a "hardness" parameter (the intensity of the smudge).  CHECK
 * Improve on the artist workflow (i.e. how the artist interacts with the tool).  ALLOW CHANGING OF SIZE WITH THE SCROLL WHEEL

Optional bonus points:

 * Improve the brush preview visuals.  NOT SURE IT'S AN IMPROVEMENT, BUT YOU CAN SEE HARDNESS IN THE CIRCLE NOW
 * Craft a better UI to control and visualize the smudge parameters.  CHECK
 * Any additions which you think would improve the tool.


